<?php

namespace App\Auth;

use App\Utility\Utility;
use PDO;

session_start();
class Auth
{
    private $userId;
    private $email;
    private $password;
    private $confirmPassword;
    private $firstName;
    private $lastName;
    private $gender;
    private $dateOfBirth;
    private $photo;
    private $hobbie;
    private $interest;

    public function setData($data = "")
    {
        if(!empty($data['userId'])) {
            $this->userId = $data['userId'];

        }
        if(!empty($data['email'])) {
            $this->email = $data['email'];

        }
        if(!empty($data['password'])) {
            $this->password = $data['password'];
        }

        if(!empty($data['rpassword'])) {
            $this->confirmPassword = $data['rpassword'];
        }

        if(!empty($data['fName'])) {
            $this->firstName = $data['fName'];
        }

        if(!empty($data['lName'])) {
            $this->lastName = $data['lName'];
        }

        if(!empty($data['gender'])) {
            $this->gender = $data['gender'];
        }

        if(!empty($data['dob'])) {
            $this->dateOfBirth = $data['dob'];
        }

        if(!empty($data['photo'])) {
            $this->photo = $data['photo'];
        }

        if(!empty($data['hobbie'])) {
            $this->hobbie = $data['hobbie'];
        }

        if(!empty($data['interest'])) {
            $this->interest = $data['interest'];
        }

        return $this;
    }

    public function passCheck()
    {
        if ( $this->password == $this->confirmPassword)
        {
            return $this;
        }
        else
        {
            header('location: registration.php');
        }
    }

    public function storePhoto()
    {
        $target = "../assets/user-profile-picture/".basename($_FILES['image']['name']);
        $db = mysqli_connect("localhost", "root", "", "amarproshno_db");
    }

    public function store()
    {

        $pdo = new PDO('mysql:host=localhost;dbname=amarproshno_db', 'root', '');

        if(isset($_POST['submitRegistration']))
        {
            $stmt = $pdo->prepare('INSERT INTO users(email, password, created_at) VALUES(:email, :password, :cat)');

            $status = $stmt->execute(
                array(
                    ':email' => $this->email,
                    ':password' => $this->password,
                    ':cat' => date('Y-m-d h:m:s')
                )
            );
            if ($status) {
                $this->login();
            }
            else
            {
                echo "jay nai";
            }
        }
        elseif(isset($_POST['submitEdit']))
        {
            $stmt = $pdo->prepare('INSERT INTO profiles( user_id, first_name, last_name, gender, dob, photo, hobbie, interest, created_at)
            VALUES(:uid, :fname, :lname, :gender, :dob, :photo, :hobbie, :interest, :cat)');

            $status = $stmt->execute(
                array(
                    ':uid' => $this->userId,
                    ':fname' => $this->firstName,
                    ':lname' => $this->lastName,
                    ':gender' => $this->gender,
                    ':dob' => $this->dateOfBirth,
                    ':photo' => $this->photo,
                    ':hobbie' => $this->hobbie,
                    ':interest' => $this->interest,
                    ':cat' => date('Y-m-d h:m:s')
                )
            );
            if ($status) {
                header('location: ../index.php');
            }
            else
            {
                echo "jay nai";
            }
        }
    }

    public function getAUser($id='')
    {
        $pdo = new PDO('mysql:host=localhost;dbname=amarproshno_db', 'root', '');
        $query = "SELECT * FROM `profiles` WHERE user_id=$id";
        $stmt = $pdo->prepare($query);
        $stmt-> execute();
        $list = $stmt->fetch();
        return $list;
    }

    public function login()
    {
        $pdo = new PDO('mysql:host=localhost;dbname=amarproshno_db', 'root', '');
        $query = "SELECT * FROM `users` WHERE email='$this->email' AND password='$this->password'";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $result = $stmt->fetch();


        if (!empty($result))
        {
            $_SESSION['user'] = $result;
            if(isset($_POST['submitRegistration']))
            {
                header('location: edit-profile.php');
            }
            else
            {
                header('location: ../index.php');
            }
        }
        else
        {
            echo "login hoy nai";
        }
    }
}