<?php

namespace App\Question;
use App\Utility\Utility;
use PDO;
class Question
{
    private $questionHead;
    private $questionDetail;

    public function setData($data = "")
    {
        if(!empty($data['qHead'])) {
            $this->questionHead = $data['qHead'];
        }
        if(!empty($data['qDetail'])) {
            $this->questionDetail = $data['qDetail'];
        }
        return $this;
    }

    public function store()
    {
        $pdo = new PDO('mysql:host=localhost;dbname=amarproshno_db', 'root', '');

        $stmt = $pdo->prepare('INSERT INTO questions(user_id, q_head, question, created_at) VALUES(:uid, :qHead, :qDetail, :cat)');

        $status = $stmt->execute(
            array(
                ':uid' => $_SESSION['user']['id'],
                ':qHead' => $this->questionHead,
                ':qDetail' => $this->questionDetail,
                ':cat' => date('Y-m-d h:m:s')
            )
        );

        if ($status) {
            header('location: ../index.php');
        }
        else
        {
            header('location: post-question.php');
        }
    }

    public function getAllQuestion()
    {
        $pdo = new PDO('mysql:host=localhost;dbname=amarproshno_db', 'root', '');
        $query = 'SELECT * FROM `questions` ORDER BY id DESC';
        $stmt = $pdo->prepare($query);
        $stmt-> execute();
        $list = $stmt->fetchAll();
        return $list;
    }

    public function getAQuestion($id='')
    {

        $pdo = new PDO('mysql:host=localhost;dbname=amarproshno_db', 'root', '');
        $query = "SELECT * FROM `questions` WHERE id=$id";
        $stmt = $pdo->prepare($query);
        $stmt-> execute();
        $list = $stmt->fetch();
        return $list;
    }
}