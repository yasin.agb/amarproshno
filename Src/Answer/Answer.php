<?php

namespace  App\Answer;
use App\Utility\Utility;
use PDO;

class Answer
{
    private $answer;
    private $questionId;

    public function setData($data = "")
    {
        if(!empty($data['questionId'])) {
            $this->questionId = $data['questionId'];
        }
        if(!empty($data['answer'])) {
            $this->answer = $data['answer'];
        }
        return $this;
    }

    public function store()
    {
        $pdo = new PDO('mysql:host=localhost;dbname=amarproshno_db', 'root', '');

        $stmt = $pdo->prepare('INSERT INTO answers (user_id, question_id, answer, created_at) VALUES(:uid, :qid, :answer, :cat)');

        $status = $stmt->execute(
            array(
                ':uid' => $_SESSION['user']['id'],
                ':qid' => $this->questionId,
                ':answer' => $this->answer,
                ':cat' => date('Y-m-d h:m:s')
            )
        );

        if ($status) {
            header("location: ../Views/full-question.php?id=$this->questionId");
        }
        else
        {
            header("location: ../Views/full-question.php?id=$this->questionId");
        }
    }

    public function getAllAnswer($id='')
    {
        $pdo = new PDO('mysql:host=localhost;dbname=amarproshno_db', 'root', '');
        $query = "SELECT * FROM `answers` WHERE question_id=$id";
        $stmt = $pdo->prepare($query);
        $stmt-> execute();
        $list = $stmt->fetchAll();
        return $list;
    }
}