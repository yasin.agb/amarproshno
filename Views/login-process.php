<?php

include_once ("../vendor/autoload.php");

use App\Auth\Auth;

$objAuth = new Auth();

$objAuth->setData($_POST)->login();