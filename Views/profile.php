<?php

include_once ("../vendor/autoload.php");
use App\Auth\Auth;
use App\Utility\Utility;

$objAuth = new Auth();
$aUserProfile = $objAuth->getAUser($_SESSION['user']['id']);

if (!isset($_SESSION['user']))
{
    header('location: login.php');
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Profile | Amar Proshno</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap styles -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet"/>

    <!-- Customize styles -->
    <link href="../style.css" rel="stylesheet"/>

    <!-- font awesome styles -->
    <link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--[if IE 7]>
    <link href="../assets/font-awesome/css/font-awesome-ie7.min.css" rel="stylesheet">
    <![endif]-->

    <!-- Favicons -->
    <link rel="shortcut icon" href="../assets/img/favicon.png">
</head>


<body>
<!--
	Upper Header Section
-->
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="topNav">
        <div class="container">
            <div class="alignR">
                <div class="pull-left socialNw">
                    <p>Amar Proshno</p>
                </div>
                <a href="../index.php"> <span class="icon-home"></span> Home</a>

                <?php
                if (isset($_SESSION['user'])) { ?>
                    <a href="profile.php"><span class="icon-user"></span> My Account</a>
                <?php } else { ?>
                    <a class="active" href = "../Views/login.php" ><span class="icon-edit" ></span > Login </a >
                <?php } ?>
                <?php
                if (isset($_SESSION['user'])) { ?>
                    <a class="active" href = "../Views/logout.php" ><span class="icon-edit" ></span > Log Out </a >
                <?php } else { ?>
                    <a class="active" href = "../Views/registration.php" ><span class="icon-edit" ></span > Free Register </a >
                <?php } ?>

                <a href="#"><span class="icon-envelope"></span> Contact us</a>
            </div>
        </div>
    </div>
</div>

<!--
Lower Header Section
-->
<div class="container">
    <div id="gototop"> </div>
    <header id="header">
        <div class="row">

        </div>
    </header>

    <!--
    Navigation Bar Section
    -->
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container">
                <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="nav-collapse">
                    <ul class="nav">
                        <li class=""><a href="../index.php">Home</a></li>
                        <li class=""><a href="../Views/post-question.php">Ask Question</a></li>
                        <li class=""><a href="#">Top Stories</a></li>
                        <li class=""><a href="#">New Questions</a></li>
                        <li class=""><a href="#">Your Questions</a></li>
                        <li class=""><a href="#">About Us</a></li>
                    </ul>
                    <form action="#" class="navbar-search pull-left">
                        <input type="text" placeholder="Search" class="search-query span2">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="span12" id="remove-left-margin">
        <ul class="breadcrumb">
            <li><a href="../index.php">Home</a> <span class="divider">/</span></li>
            <li class="active">Profile</li>
        </ul>
    </div>



    <div class="row">

        <div id="sidebar" class="span3">
            <div class="well well-small">
                <ul class="nav nav-list">
                    <li><h3>Categories</h3></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Beauty & Style</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Business & Finance</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Computers & Internet</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Education</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Games & Recreation Health</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Politics & Government</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Science & Mathematics</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Social Science</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Society & Culture</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Sports</a></li>
                </ul>
            </div>
        </div>

        <div class="span9">
            <div class="well well-small">
                <div class="row-fluid">
                    <div class="span12">
                        <h2 style="text-align: center">Its your profile <?php echo $aUserProfile['first_name']?></h2>
                        <img src="../assets/img/user.png" style="width: 120px; margin: 0 auto; padding-left: 40%">
                    </div>
                </div>
                <hr class="soften">
                <div class="row-fluid">
                    <div class="span12">
                        <h4><table>
                            <tr>
                                <td>Full Name : </td>
                                <td> <?php echo $aUserProfile['first_name']." ".$aUserProfile['last_name']?></td>
                            </tr>
                            </table></h4>
                    </div>
                </div>
                <hr class="soften">
                <div class="row-fluid">
                    <div class="span12">
                        <h4><table>
                                <tr>
                                    <td>Email : </td>
                                    <td><?php echo $_SESSION['user']['email'] ?></td>
                                </tr>
                            </table></h4>
                    </div>
                </div>
                <hr class="soften">
                <div class="row-fluid">
                    <div class="span12">
                        <h4><table>
                                <tr>
                                    <td>Gender : </td>
                                    <td> <?php echo $aUserProfile['gender'] ?></td>
                                </tr>
                            </table></h4>
                    </div>
                </div>
                <hr class="soften">
                <?php if ($aUserProfile['dob'] != 0000-00-00){ ?>
                <div class="row-fluid">
                    <div class="span12">
                        <h4><table>
                                <tr>
                                    <td>Date of Birth : </td>
                                    <td> <?php echo $aUserProfile['dob'] ?></td>
                                </tr>
                            </table></h4>
                    </div>
                </div>
                <hr class="soften">
                <?php }
                if (!empty($aUserProfile['hobbie'])) {
                ?>
                <div class="row-fluid">
                    <div class="span12">
                        <h4><table>
                                <tr>
                                    <td>Hobbie :</td>
                                    <td> <?php echo $aUserProfile['hobbie'] ?></td>
                                </tr>
                            </table></h4>
                    </div>
                </div>
                <hr class="soften">
                <?php }
                if (!empty($aUserProfile['interest'])) {
                ?>
                <div class="row-fluid">
                    <div class="span12">
                        <h4><table>
                                <tr>
                                    <td>interest : </td>
                                    <td> <?php echo $aUserProfile['interest'] ?></td>
                                </tr>
                            </table></h4>
                    </div>
                </div>
                <hr class="soften">
                <?php } ?>

                <div class="row-fluid">
                    <div class="span12">
                        <a href="#"><button class="exclusive shopBtn">Update Profile</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery.easing-1.3.min.js"></script>
    <script src="../assets/js/jquery.scrollTo-1.4.3.1-min.js"></script>
    <script src="../assets/js/shop.js"></script>
</body>
</html>