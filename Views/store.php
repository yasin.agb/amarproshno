<?php

include_once ("../vendor/autoload.php");

use App\Auth\Auth;
use App\Question\Question;
use App\Answer\Answer;


$objAuth = new Auth();
$objQuestion = new Question();
$objAnswer = new Answer();


if (isset($_POST['submitRegistration'])) {
    $objAuth->setData($_POST)->passCheck()->store();
}
if (isset($_POST['submitEdit'])) {
    $objAuth->setData($_POST)->store();
}
elseif (isset($_POST['postQuestion'])) {
    $objQuestion->setData($_POST)->store();
}
elseif (isset($_POST['answerSubmit'])) {
    $objAnswer->setData($_POST)->store();
}