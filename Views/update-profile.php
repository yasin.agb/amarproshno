<?php

include_once ("../vendor/autoload.php");
use App\Auth\Auth;
use App\Utility\Utility;

$objAuth = new Auth();
$aUserProfile = $objAuth->getAUser($_SESSION['user']['id']);

if (!isset($_SESSION['user']))
{
    header('location: login.php');
}
//Utility::dd($aUserProfile);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Edit Profile | Amar Proshno</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap styles -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet"/>

    <!-- Customize styles -->
    <link href="../style.css" rel="stylesheet"/>

    <!-- font awesome styles -->
    <link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--[if IE 7]>
    <link href="../assets/font-awesome/css/font-awesome-ie7.min.css" rel="stylesheet">
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicons -->
    <link rel="shortcut icon" href="../assets/img/favicon.png">
</head>


<body>
<!--
	Upper Header Section
-->
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="topNav">
        <div class="container">
            <div class="alignR">
                <div class="pull-left socialNw">
                    <p>Amar Proshno</p>
                </div>
                <a href="../index.php"> <span class="icon-home"></span> Home</a>

                <?php
                if (isset($_SESSION['user'])) { ?>
                    <a href="profile.php"><span class="icon-user"></span> My Account</a>
                <?php } else { ?>
                    <a class="active" href = "../Views/login.php" ><span class="icon-edit" ></span > Login </a >
                <?php } ?>
                <?php
                if (isset($_SESSION['user'])) { ?>
                    <a class="active" href = "../Views/logout.php" ><span class="icon-edit" ></span > Log Out </a >
                <?php } else { ?>
                    <a class="active" href = "../Views/registration.php" ><span class="icon-edit" ></span > Free Register </a >
                <?php } ?>

                <a href="#"><span class="icon-envelope"></span> Contact us</a>
            </div>
        </div>
    </div>
</div>

<!--
Lower Header Section
-->
<div class="container">
    <div id="gototop"> </div>
    <header id="header">
        <div class="row">

        </div>
    </header>

    <!--
    Navigation Bar Section
    -->
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container">
                <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="nav-collapse">
                    <ul class="nav">
                        <li class=""><a href="../index.php">Home</a></li>
                        <li class=""><a href="../Views/post-question.php">Ask Question</a></li>
                        <li class=""><a href="#">Top Stories</a></li>
                        <li class=""><a href="#">New Questions</a></li>
                        <li class=""><a href="#">Your Questions</a></li>
                        <li class=""><a href="#">About Us</a></li>
                    </ul>
                    <form action="#" class="navbar-search pull-left">
                        <input type="text" placeholder="Search" class="search-query span2">
                    </form>
                </div>
            </div>
        </div>
    </div>    <!--
    Body Section
    -->

    <div class="span12" id="remove-left-margin">
        <ul class="breadcrumb">
            <li><a href="../index.php">Home</a> <span class="divider">/</span></li>
            <li class="active">Edit Profile</li>
        </ul>

        <hr class="soft"/>
        <div class="well">

            <form action="store.php" method="post" class="form-horizontal">
                <h3>Your Personal Details</h3>
                <div class="control-group">
                    <label class="control-label" for="inputFname">First name <sup>*</sup></label>
                    <div class="controls">
                        <input type="text" name="fName" id="inputFname" placeholder="First Name" value="<?php echo $aUserProfile['first_name'] ?>" required>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputLname">Last name <sup>*</sup></label>
                    <div class="controls">
                        <input type="text" name="lName" id="inputLname" placeholder="Last Name" value="<?php echo $aUserProfile['last_name'] ?>" required>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="gender">Gender <sup>*</sup></label>
                    <div class="controls">
                        <input type="radio" name="gender" id="gender" value="Male" <?php if($aUserProfile['gender'] == "Male") ?> required> Male
                        <input type="radio" name="gender" id="gender" value="Female"> Female
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="dob">Dete of Birth</label>
                    <div class="controls">
                        <input type="date" name="dob" id="dob"> <?php
                        if(stripos($_SERVER['HTTP_USER_AGENT'], "Chrome") == false)
                            echo "(YYYY-MM-DD)" ?>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="photo">Profile Picture</label>
                    <div class="controls">
                        <input type="file" name="photo" id="photo">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="hobbie">Hobbies</label>
                    <div class="controls">
                        <textarea name="hobbie" id="hobbie"></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="interest">Interests</label>
                    <div class="controls">
                        <textarea name="interest" id="interest"></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <input type="hidden" name="userId" value="<?php echo $_SESSION['user']['id']?>">
                        <input type="submit" name="submitEdit" value="Save" class="exclusive shopBtn">
                        <input type="reset" value="Reset" class="exclusive shopBtn">
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

</div><!-- /container -->




<a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
<!-- Placed at the end of the document so the pages load faster -->
<script src="../assets/js/jquery.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/jquery.easing-1.3.min.js"></script>
<script src="../assets/js/jquery.scrollTo-1.4.3.1-min.js"></script>
<script src="../assets/js/shop.js"></script>
</body>
</html>