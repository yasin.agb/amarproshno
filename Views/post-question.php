<?php session_start() ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Post Question | Amar Proshno</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap styles -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet"/>

    <!-- Customize styles -->
    <link href="../style.css" rel="stylesheet"/>

    <!-- font awesome styles -->
    <link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--[if IE 7]>
    <link href="../assets/font-awesome/css/font-awesome-ie7.min.css" rel="stylesheet">
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="https://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>

    <!-- Favicons -->
    <link rel="shortcut icon" href="../assets/img/favicon.png">
</head>


<body>
<!--
	Upper Header Section
-->
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="topNav">
        <div class="container">
            <div class="alignR">
                <div class="pull-left socialNw">
                    <p>Amar Proshno</p>
                </div>
                <a href="../index.php"> <span class="icon-home"></span> Home</a>

                <?php
                if (isset($_SESSION['user'])) { ?>
                    <a href="#"><span class="icon-user"></span> My Account</a>
                <?php } else { ?>
                    <a class="active" href = "../Views/login.php" ><span class="icon-edit" ></span > Login </a >
                <?php } ?>
                <?php
                if (isset($_SESSION['user'])) { ?>
                    <a class="active" href = "../Views/logout.php" ><span class="icon-edit" ></span > Log Out </a >
                <?php } else { ?>
                    <a class="active" href = "../Views/registration.php" ><span class="icon-edit" ></span > Free Register </a >
                <?php } ?>

                <a href="#"><span class="icon-envelope"></span> Contact us</a>
            </div>
        </div>
    </div>
</div>

<!--
Lower Header Section
-->
<div class="container">
    <div id="gototop"> </div>
    <header id="header">
        <div class="row">

        </div>
    </header>

    <!--
    Navigation Bar Section
    -->
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container">
                <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="nav-collapse">
                    <ul class="nav">
                        <li class=""><a href="../index.php">Home</a></li>
                        <li class="active"><a href="../Views/post-question.php">Ask Question</a></li>
                        <li class=""><a href="#">Top Stories</a></li>
                        <li class=""><a href="#">New Questions</a></li>
                        <li class=""><a href="#">Your Questions</a></li>
                        <li class=""><a href="#">About Us</a></li>
                    </ul>
                    <form action="#" class="navbar-search pull-left">
                        <input type="text" placeholder="Search" class="search-query span2">
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!--
    Body Section
    -->
    <div class="row">
        <div id="sidebar" class="span3">
            <div class="well well-small">
                <ul class="nav nav-list">
                    <li><h3>Categories</h3></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Beauty & Style</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Business & Finance</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Computers & Internet</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Education</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Games & Recreation Health</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Politics & Government</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Science & Mathematics</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Social Science</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Society & Culture</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Sports</a></li>
                </ul>
            </div>
        </div>



        <div class="span9">
            <ul class="breadcrumb">
                <li><a href="../index.php">Home</a> <span class="divider">/</span></li>
                <li class="active">Ask Question</li>
            </ul>
            <div class="well well-small">
                <h3><?php if (!isset($_SESSION['user'])) { echo "You Need Login to Question"; }
                    else {echo "Post Your Question"; } ?></h3>
                <hr class="soft"/>

                <form action="store.php" method="post" class="form-inline">
                    <label class="control-label" for="qHead">Question Title: </label>
                    <input type="text" name="qHead" style="width: 80%;" class="span4" id="qHead" placeholder="Type your question title" <?php if (!isset($_SESSION['user'])) { echo "disabled"; } ?> required>
                    <br><br>
                    <label class="control-label">Question details: </label>
                    <textarea name="qDetail" <?php if (!isset($_SESSION['user'])) { echo "disabled"; } ?> required></textarea><br>
                    <button type="submit" class="shopBtn block" value="postQuestion" name="postQuestion">Post</button>
                </form>
            </div>
        </div>

    </div>


</div><!-- /container -->




<a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
<!-- Placed at the end of the document so the pages load faster -->
<script>
    CKEDITOR.replace( 'qDetail' );
</script>
<script src="../assets/js/jquery.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/jquery.easing-1.3.min.js"></script>
<script src="../assets/js/jquery.scrollTo-1.4.3.1-min.js"></script>
<script src="../assets/js/shop.js"></script>
</body>
</html>