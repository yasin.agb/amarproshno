<?php

include_once ("vendor/autoload.php");
use App\Question\Question;

$objQustion = new Question();
$allQuestion = $objQustion->getAllQuestion();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Home | Amar Proshno</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>

    <!-- Customize styles -->
    <link href="style.css" rel="stylesheet"/>

    <!-- font awesome styles -->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--[if IE 7]>
    <link href="assets/font-awesome/css/font-awesome-ie7.min.css" rel="stylesheet">
    <![endif]-->

    <!-- Favicons -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
</head>


<body>
<!--
	Upper Header Section
-->
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="topNav">
        <div class="container">
            <div class="alignR">
                <div class="pull-left socialNw">
                    <p>Amar Proshno</p>
                </div>
                <a href="index.php"> <span class="icon-home"></span> Home</a>

                <?php
                session_start();
                if (isset($_SESSION['user'])) { ?>
                    <a href="Views/profile.php"><span class="icon-user"></span> My Account</a>
                <?php } else { ?>
                    <a class="active" href = "Views/login.php" ><span class="icon-edit" ></span > Login </a >
                <?php } ?>
                <?php
                if (isset($_SESSION['user'])) { ?>
                    <a class="active" href = "Views/logout.php" ><span class="icon-edit" ></span > Log Out </a >
                <?php } else { ?>
                <a class="active" href = "Views/registration.php" ><span class="icon-edit" ></span > Free Register </a >
                <?php } ?>

                <a href="#"><span class="icon-envelope"></span> Contact us</a>
            </div>
        </div>
    </div>
</div>

<!--
Lower Header Section
-->
<div class="container">
    <div id="gototop"> </div>
    <header id="header">
        <div class="row">

        </div>
    </header>

    <!--
    Navigation Bar Section
    -->
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container">
                <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="nav-collapse">
                    <ul class="nav">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li class=""><a href="Views/post-question.php">Ask Question</a></li>
                        <li class=""><a href="#">Top Stories</a></li>
                        <li class=""><a href="#">New Questions</a></li>
                        <li class=""><a href="#">Your Questions</a></li>
                        <li class=""><a href="#">About Us</a></li>
                    </ul>
                    <form action="#" class="navbar-search pull-left">
                        <input type="text" placeholder="Search" class="search-query span2">
                    </form>
                </div>
            </div>
        </div>
    </div>



    <div class="row">

        <div id="sidebar" class="span3">
            <div class="well well-small">
                <ul class="nav nav-list">
                    <li><h3>Categories</h3></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Beauty & Style</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Business & Finance</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Computers & Internet</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Education</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Games & Recreation Health</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Politics & Government</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Science & Mathematics</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Social Science</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Society & Culture</a></li>
                    <li><a href="#"><span class="icon-chevron-right"></span>Sports</a></li>
                </ul>
            </div>
        </div>

        <div class="span9">
            <div class="well well-small">

                <?php foreach($allQuestion as $item){?>
                <div class="row-fluid">
                    <div class="span1">
                        <img src="assets/img/discuss-issue.png" style="padding: 20px 15px">
                    </div>
                    <div class="span8">
                        <h4><?php echo $item['q_head'] ?></h4>
                        <p>
                            <?php echo $item['question'] ?>
                        </p>
                    </div>
                    <div class="span3 alignR">
                        <form class="form-horizontal qtyFrm">
                            <h3></h3>

                            <div class="btn-group">
                                <a href="Views/full-question.php?id=<?php echo $item['id'] ?>" class="shopBtn">VIEW MORE</a>
                            </div>
                        </form>
                    </div>
                </div>
                    <hr class="soften">
                <?php }?>


            </div>
        </div>
    </div>


    <a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.easing-1.3.min.js"></script>
    <script src="assets/js/jquery.scrollTo-1.4.3.1-min.js"></script>
    <script src="assets/js/shop.js"></script>
</body>
</html>